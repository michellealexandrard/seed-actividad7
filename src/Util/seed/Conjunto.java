/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T> {

    private ListaS<T> elementos;

    public Conjunto() {
        this.elementos = new ListaS();
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> union = new Conjunto();
        union.agregarConjunto(this);
        union.agregarConjunto(c1);
        return union;
    }

    public void agregarConjunto(Conjunto<T> conjunto) {
        for (T elemento : conjunto.elementos) {
            this.agregarSinRepetidos(elemento);
        }
    }

    public void agregarSinRepetidos(T elemento) {
        if (!this.elementos.contains(elemento)) {
            this.elementos.insertarFin(elemento);
        }
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        if (this.elementos.getSize() < c1.elementos.getSize()) {
            mayor = c1;
            menor = this;
        }
        for (T elemento : menor.elementos) {
            if (mayor.elementos.contains(elemento)) {
                interseccion.agregarSinRepetidos(elemento);
            }
        }
        return interseccion;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> diferencia = new Conjunto();
        for (T elemento : this.elementos) {
            if (!c1.elementos.contains(elemento)) {
                diferencia.agregarSinRepetidos(elemento);
            }
        }
        return diferencia;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> diferenciaAsimetrica = new Conjunto<T>();
        diferenciaAsimetrica = getDiferencia(c1);
        Conjunto<T> diferencia2 = c1.getDiferencia(this);
        //difeAsi.agregarConjunto(diferencia2);
        diferenciaAsimetrica.elementos.addAll(diferencia2.elementos);
        return diferenciaAsimetrica;
    }

    public Conjunto<Conjunto<T>> getConjuntoPotencia() {
        Conjunto<Conjunto<T>> conjuntoPotencia = new Conjunto();
        int tamaño = (int) Math.pow(2, this.elementos.getSize());
        for (int i = 0; i < tamaño; i++) {
            Conjunto<T> subconjunto = new Conjunto();
            String binario = String.format("%0" + this.elementos.getSize() + "d", Integer.parseInt(Integer.toBinaryString(i)));
            for (int k = 0; k < binario.length(); k++) {
                if (binario.charAt(k) == '1') {
                    subconjunto.elementos.insertarFin(this.elementos.get(k));
                }
            }
            conjuntoPotencia.elementos.insertarFin(subconjunto);
        }
        return conjuntoPotencia;
    }

    @Override
    public T get(int i) {
        return this.elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        this.elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        this.elementos.insertarFin(info);
    }

    public String toString() {
        String result = "{";
        if (this.elementos.isVacia()) {
            return "{*}";
        } else {
            for (int i = 0; i < this.elementos.getSize(); i++) {
                result += this.elementos.get(i);
                if (i < this.elementos.getSize() - 1) {
                    result += ",";
                }
            }
        }
        result += "}";
        return result;
    }

}
