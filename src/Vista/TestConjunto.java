/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.seed.Conjunto;
import java.util.Random;

/**
 *
 * @author Michelle Rojas
 */
public class TestConjunto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random random = new Random();
        // Se crean dos conjuntos con elementos aleatorios
        Conjunto<Integer> conjunto1 = new Conjunto<>();
        for (int i = 0; i < 3; i++) {
            conjunto1.insertar(random.nextInt(10));
        }

        Conjunto<Integer> conjunto2 = new Conjunto<>();
        for (int i = 0; i < 3; i++) {
            conjunto2.insertar(random.nextInt(10));
        }

        // Se muestran los elementos de cada conjunto
        System.out.println("Conjunto 1: " + conjunto1);
        System.out.println("Conjunto 2: " + conjunto2);

//        //Se prueban los get,set e insertar
//        System.out.println("get: " + conjunto1.get(2));
//        conjunto1.set(3, 588);
//        System.out.println("set: " + conjunto1);
//        conjunto1.insertar(100);
//        System.out.println("Insertar: " + conjunto1);
//        
//        System.out.println("Conjunto 1: " + conjunto1);
//        System.out.println("Conjunto 2: " + conjunto2);
        // Se obtiene y se muestra la unión de los conjuntos
        Conjunto<Integer> union = conjunto1.getUnion(conjunto2);
        System.out.println("Unión de los conjuntos: " + union);

        // Se obtiene y se muestra la Interseccion de los conjuntos
        Conjunto<Integer> interseccion = conjunto1.getInterseccion(conjunto2);
        System.out.println("Interseccion de los conjuntos: " + interseccion);

        // Se obtiene y se muestra la diferencia de los conjuntos
        Conjunto<Integer> diferencia = conjunto1.getDiferencia(conjunto2);
        System.out.println("Dferencia de conjuntos: " + diferencia);

        // Se obtiene y se muestra la diferencia asimetrica de los conjuntos
        Conjunto<Integer> diferenciaAsimetrica = conjunto1.getDiferenciaAsimetrica(conjunto2);
        System.out.println("Dferencia Asimetrica de conjuntos: " + diferenciaAsimetrica);

        // Se obtiene y se muestra el conjunto potencia
        Conjunto<Conjunto<Integer>> conjuntoPotencia = conjunto1.getConjuntoPotencia();
        System.out.println("Conjuntos potencia: " + conjuntoPotencia);
    }

}
