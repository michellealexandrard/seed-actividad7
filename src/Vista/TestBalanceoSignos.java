/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author DOCENTE
 */
public class TestBalanceoSignos {

    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V L={[aa]} => --> F L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        System.out.println("Digite la cadena a validar:");
        String s = t.nextLine();
        
        if (validarCadena(s)) {
            System.out.println("Cumple :)");
        } else {
            System.out.println("No cumple :(");
        }
    }

    public static boolean validarCadena(String s) {
        Pila<Character> pila = new Pila();
        int tieneSimbolos = 0; //simbolos de apertura [ { ( y simbolos de cierre ) } ]
        boolean esValida = true;
        for (int i = 0; i < s.length() && esValida; i++) {
            char c = s.charAt(i);
            if (c == '[' || c == '{' || c == '(') {
                pila.push(c);
                tieneSimbolos++;
                if (pila.size() > 1) {
                    esValida = validarSimbolos(pila.clone());
                }
            }
            if (c == ']' || c == '}' || c == ')' && esValida) {
                tieneSimbolos++;
                if (pila.isVacia()) {
                    esValida = false;
                } else {
                    char b = pila.pop();
                    if ((c == ']' && b != '[') || (c == '}' && b != '{') || (c == ')' && b != '(')) {
                        esValida = false;
                    }
                }
            }
        }
        if (tieneSimbolos < 2 || s.length() == 0) {
            throw new RuntimeException("La cadena ingresada es inválida");
        }
        return esValida;
    }

    public static boolean validarSimbolos(Pila<Character> pilaCopy) {
        char ultima = pilaCopy.pop();
        char anterior = pilaCopy.pop();
        return (ultima == '{' && anterior == '[') || (ultima == '(' && (anterior == '{' || anterior == '['));
    }
}
